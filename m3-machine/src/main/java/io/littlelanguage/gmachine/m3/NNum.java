package io.littlelanguage.gmachine.m3;

public class NNum extends Node {
    private int n;
    private Node ind;


    public NNum(int n) {
        this.n = n;
        ind = null;
    }

    @Override
    public String show(int depth) {
        if (depth == 0)
            return "...";
        else if (ind == null)
            return "NNum(n=" + n + ")";
        else
            return "NInd(e=" + ind.show(depth - 1) + ")";
    }

    public int n() {
        if (ind == null)
            return n;
        else
            throw new IllegalStateException("Attempt to get n from NInd: " + toString());
    }

    @Override
    public boolean isInd() {
        return ind != null;
    }

    @Override
    public Node indNode() {
        if (ind == null)
            throw new IllegalStateException("Attempt to get ind from NNum: " + toString());
        else
            return ind;
    }

    @Override
    public void mkInd(Node node) {
        ind = node;
    }
}
