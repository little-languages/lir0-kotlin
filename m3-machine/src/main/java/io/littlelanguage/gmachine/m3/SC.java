package io.littlelanguage.gmachine.m3;

public interface SC {
    void run(Machine machine);

    int numberOfParameters();
}
