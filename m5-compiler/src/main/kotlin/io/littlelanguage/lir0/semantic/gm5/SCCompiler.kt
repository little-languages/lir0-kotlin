package io.littlelanguage.lir0.semantic.gm5

import io.littlelanguage.lir0.dynamic.ast.*
import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

class SCCompiler(override val className: ClassName) : ParentCompiler(className) {
    fun sc(scDefn: SCDefn): ByteArray {
        cw.visit(Opcodes.V1_5, Opcodes.ACC_PUBLIC, className.descriptor, null, "java/lang/Object", arrayOf("io/littlelanguage/gmachine/m5/SC"))

        cw.visitField(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC + Opcodes.ACC_FINAL, "INSTANCE", "L${className.descriptor};", null, null).visitEnd()

        addClassConstructor()
        addDefaultConstructor()
        addToString()

        if (scDefn.first == "main") {
            addMainFunction()
        }

        addNumberOfParameters(scDefn.second.size)

        compileR(scDefn.second, scDefn.third)

        cw.visitEnd()

        return cw.toByteArray()
    }

    private fun addMainFunction() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null)

        mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;")
        mv.visitTypeInsn(Opcodes.NEW, "io/littlelanguage/gmachine/m5/Machine")
        mv.visitInsn(Opcodes.DUP)
        mv.visitVarInsn(Opcodes.BIPUSH, 100)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "io/littlelanguage/gmachine/m5/Machine", "<init>", "(I)V", false)
        mv.visitFieldInsn(Opcodes.GETSTATIC, className.descriptor, "INSTANCE", "L${className.descriptor};")
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m5/Machine", "run", "(Lio/littlelanguage/gmachine/m5/SC;)Lio/littlelanguage/gmachine/m5/Node;", false)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/Object;)V", false)
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun compileR(names: List<String>, expr: Expr) {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "run", "(Lio/littlelanguage/gmachine/m5/Machine;)V", null, null)

        compileE(expr, Bindings(names.zip(0..names.size).toMap()), mv)
        mv.generateUpdate(names.size)
        mv.generatePop(names.size)
        mv.generateUnwind()
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun compileE(e: Expr, b: Bindings, mv: MethodVisitor): Unit {
        if (e is EAp) {
            val ee1 = e.e1

            if (ee1 is EAp) {
                val ee1e1 = ee1.e1

                if (ee1e1 is EVar) {
                    val instructions =
                            builtInDyadic[ee1e1.name]

                    if (instructions != null) {
                        compileE(e.e2, b, mv)
                        compileE(ee1.e2, b.deltaOffset(1), mv)
                        instructions(mv)
                        return
                    }
                } else if (ee1e1 is EAp) {
                    val ee1e1e1 =
                            ee1e1.e1

                    if (ee1e1e1 is EVar && ee1e1e1.name == "if") {
                        val elseLabel =
                                Label()

                        val endLabel =
                                Label()

                        compileE(ee1e1.e2, b, mv)
                        mv.visitVarInsn(Opcodes.ALOAD, 1)
                        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m5/Machine", "cond", "()Z", false)
                        mv.visitJumpInsn(Opcodes.IFEQ, elseLabel)

                        compileE(ee1.e2, b, mv)
                        mv.visitJumpInsn(Opcodes.GOTO, endLabel)

                        mv.visitLabel(elseLabel)
                        compileE(e.e2, b, mv)
                        mv.visitJumpInsn(Opcodes.GOTO, endLabel)

                        mv.visitLabel(endLabel)
                        return;
                    }
                }
            } else if (ee1 is EVar && ee1.name == "negate") {
                compileE(e.e2, b, mv)
                mv.generateNeg()
                return
            }
        }

        return when {
            e is ENum -> mv.generatePushInt(e.value)
            e is ELet ->
                when (e.isRec) {
                    IsRec.NonRecursive -> {
                        var bindings =
                                b

                        e.decls.forEach {
                            compileC(it.second, bindings, mv)
                            bindings = bindings.deltaOffset(1)
                        }

                        e.decls.forEachIndexed { index, pair ->
                            bindings = bindings.add(pair.first, e.decls.size - index - 1)
                        }

                        compileE(e.expr, bindings, mv)

                        mv.generateSlide(e.decls.size)
                    }
                    IsRec.Recursive -> {
                        var bindings =
                                b.deltaOffset(e.decls.size)

                        mv.generateAlloc(e.decls.size)

                        e.decls.forEachIndexed { index, pair ->
                            bindings = bindings.add(pair.first, e.decls.size - index - 1)
                        }

                        e.decls.forEachIndexed { index, pair ->
                            compileC(pair.second, bindings, mv)
                            mv.generateUpdate(e.decls.size - index - 1)
                        }

                        compileE(e.expr, bindings, mv)

                        mv.generateSlide(e.decls.size)
                    }
                }
            else -> {
                compileC(e, b, mv)
                mv.generateEval()
            }
        }
    }

    private fun compileC(e: Expr, b: Bindings, mv: MethodVisitor): Unit =
            when (e) {
                is ENum -> mv.generatePushInt(e.value)
                is EAp -> {
                    compileC(e.e2, b, mv)
                    compileC(e.e1, b.deltaOffset(1), mv)
                    mv.generateMkAp()
                }
                is EVar -> {
                    val offset =
                            b[e.name]

                    if (offset == null) {
                        mv.generatePushGlobal(className.relativeClassName(e.name).descriptor)
                    } else
                        mv.generatePush(offset)
                }
                is ELet ->
                    when (e.isRec) {
                        IsRec.NonRecursive -> {
                            var bindings =
                                    b

                            e.decls.forEach {
                                compileC(it.second, bindings, mv)
                                bindings = bindings.deltaOffset(1)
                            }

                            e.decls.forEachIndexed { index, pair ->
                                bindings = bindings.add(pair.first, e.decls.size - index - 1)
                            }

                            compileC(e.expr, bindings, mv)

                            mv.generateSlide(e.decls.size)
                        }
                        IsRec.Recursive -> {
                            var bindings =
                                    b.deltaOffset(e.decls.size)

                            mv.generateAlloc(e.decls.size)

                            e.decls.forEachIndexed { index, pair ->
                                bindings = bindings.add(pair.first, e.decls.size - index - 1)
                            }

                            e.decls.forEachIndexed { index, pair ->
                                compileC(pair.second, bindings, mv)
                                mv.generateUpdate(e.decls.size - index - 1)
                            }

                            compileC(e.expr, bindings, mv)

                            mv.generateSlide(e.decls.size)
                        }
                    }
                else -> TODO(e.toString())
            }
}

val builtInDyadic =
        mapOf<String, (MethodVisitor) -> Unit>(
                Pair("+", { mv -> mv.generateAdd() }),
                Pair("-", { mv -> mv.generateSub() }),
                Pair("*", { mv -> mv.generateMul() }),
                Pair("/", { mv -> mv.generateDiv() }),
                Pair("==", { mv -> mv.generateEq() }),
                Pair("~=", { mv -> mv.generateNe() }),
                Pair("<", { mv -> mv.generateLt() }),
                Pair("<=", { mv -> mv.generateLe() }),
                Pair(">", { mv -> mv.generateGt() }),
                Pair(">=", { mv -> mv.generateGe() })
        )