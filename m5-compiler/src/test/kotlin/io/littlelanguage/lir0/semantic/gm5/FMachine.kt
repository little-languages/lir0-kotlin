package io.littlelanguage.lir0.semantic.gm5

import io.littlelanguage.gmachine.m5.*

class FMachine(stackSize: Int) : Machine(stackSize) {
    override fun mkAp() {
        printStack("MkAp")
        super.mkAp()
    }

    override fun push(n: Int) {
        printStack("Push $n")
        super.push(n)
    }

    override fun pushGlobal(sc: SC) {
        printStack("PushGlobal $sc")
        super.pushGlobal(sc)
    }

    override fun pushInt(n: Int) {
        printStack("PushInt $n")
        super.pushInt(n)
    }

    override fun update(n: Int) {
        printStack("Update $n")
        super.update(n)
    }

    override fun pop(n: Int) {
        printStack("Pop $n")
        super.pop(n)
    }

    override fun unwindNInd(n: Node) {
        printStack("Unwind")
        super.unwindNInd(n)
    }

    override fun unwindNAp(n: NAp) {
        printStack("Unwind")
        super.unwindNAp(n)
    }

    override fun unwindNGlobal(n: NGlobal) {
        printStack("Unwind")
        super.unwindNGlobal(n)
    }

    override fun unwindNNum(n: NNum) {
        printStack("Unwind")
        super.unwindNNum(n)
    }

    override fun slide(n: Int) {
        printStack("Slide $n")
        super.slide(n)
    }

    override fun alloc(n: Int) {
        printStack("Alloc $n")
        super.alloc(n)
    }

    override fun eval() {
        printStack("Eval")
        super.eval()
    }

    override fun add() {
        printStack("Add")
        super.add()
    }

    override fun sub() {
        printStack("Sub")
        super.sub()
    }

    override fun mul() {
        printStack("Mul")
        super.mul()
    }

    override fun div() {
        printStack("Div")
        super.div()
    }

    override fun neg() {
        printStack("Neg")
        super.neg()
    }

    override fun eq() {
        printStack("Eq")
        super.eq()
    }

    override fun ne() {
        printStack("Ne")
        super.ne()
    }

    override fun lt() {
        printStack("Lt")
        super.lt()
    }

    override fun le() {
        printStack("Le")
        super.le()
    }

    override fun gt() {
        printStack("Gt")
        super.gt()
    }

    override fun ge() {
        printStack("Ge")
        super.ge()
    }

    override fun cond(i1: SC, i2: SC) {
        printStack("Cond $i1 $i2")
        super.cond(i1, i2)
    }

    override fun cond(): Boolean {
        printStack("Cond")
        return super.cond()
    }
}
