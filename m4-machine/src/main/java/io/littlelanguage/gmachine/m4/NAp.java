package io.littlelanguage.gmachine.m4;

public class NAp extends Node {
    private Object e1;
    private Node e2;

    public NAp(Node e1, Node e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    @Override
    public String show(int depth) {
        if (depth == 0)
            return "...";
        else if (e1 == null) {
            return "NInd(e=" + e2.show(depth -1) + ")";
        } else {
            return "NAp(e1=" + ((Node) e1).show(depth - 1) + ", e2=" + e2.show(depth - 1) + ")";
        }
    }

    @Override
    public void mkInd(Node node) {
        e1 = null;
        e2 = node;
    }

    public boolean isInd() {
        return e1 == null;
    }

    public Node e1() {
        if (e1 == null)
            throw new IllegalStateException("Attempt to get e1 from NInd: " + toString());
        else
            return (Node) e1;
    }

    public Node e2() {
        if (e1 == null)
            throw new IllegalStateException("Attempt to get e2 from NInd: " + toString());
        else
            return e2;
    }

    public Node indNode() {
        if (e1 != null)
            throw new IllegalStateException("Attempt to get ind from NAp: " + toString());
        else
            return e2;
    }
}
