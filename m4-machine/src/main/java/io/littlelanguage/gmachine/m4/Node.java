package io.littlelanguage.gmachine.m4;

public abstract class Node {
    abstract public boolean isInd();
    abstract public Node indNode();
    abstract public void mkInd(Node node);

    abstract public String show(int depth);

    public String toString() {
        return show(10);
    }
}
