package io.littlelanguage.gmachine.m4;

public interface SC {
    void run(Machine machine);

    int numberOfParameters();
}
