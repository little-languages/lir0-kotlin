package io.littlelanguage.gmachine.m4;

class FMachine extends Machine {

    public FMachine(int stackSize) {
        super(stackSize);
    }

    @Override
    public void pushGlobal(SC sc) {
        printStack("pushGlobal " + sc.toString());
        super.pushGlobal(sc);
    }

    @Override
    public void pushInt(int n) {
        printStack("pushInt " + n);
        super.pushInt(n);
    }

    @Override
    public void mkAp() {
        printStack("mkAp");
        super.mkAp();
    }

    @Override
    public void push(int n) {
        printStack("push " + n);
        super.push(n);
    }

    @Override
    public void update(int n) {
        printStack("update " + n);
        super.update(n);
    }

    @Override
    public void pop(int n) {
        printStack("pop " + n);
        super.pop(n);
    }

    @Override
    public void unwindNInd(Node n) {
        printStack("unwind NInd");
        super.unwindNInd(n);
    }

    @Override
    public void unwindNAp(NAp n) {
        printStack("unwind NAp");
        super.unwindNAp(n);
    }

    @Override
    public void unwindNGlobal(NGlobal n) {
        printStack("unwind NGlobal");
        super.unwindNGlobal(n);
    }

    @Override
    public void unwindNNum(NNum n) {
        printStack("unwind NNum");
        super.unwindNNum(n);
    }

    @Override
    public void slide(int n) {
        printStack("slide " + n);
        super.slide(n);
    }

    @Override
    public void alloc(int n) {
        printStack("alloc " + n);
        super.alloc(n);
    }

    @Override
    public void eval() {
        printStack("eval");
        super.eval();
    }
}
