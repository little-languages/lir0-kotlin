package io.littlelanguage.gmachine.m4;

class S implements SC {
    public static final S INSTANCE = new S();

    @Override
    public void run(Machine m) {
        m.push(2);
        m.push(2);
        m.mkAp();
        m.push(3);
        m.push(2);
        m.mkAp();
        m.mkAp();
        m.update(3);
        m.pop(3);
        m.unwind();
    }

    @Override
    public int numberOfParameters() {
        return 3;
    }

    @Override
    public String toString() {
        return "S";
    }
}
