# LIR0 - Kotlin

LIR0 is a faithful representation of the intermediate language defined in the tutorial book [Implementing functional languages: a tutorial](https://www.microsoft.com/en-us/research/publication/implementing-functional-languages-a-tutorial/) by Simon Peyton Jones and David Lester.

This implementation includes the following features over the book's `Core Language`:

- Lexical analyses
- Static Syntax Analysis
- Dynamic Syntax Analysis
- Translation into a minimal G-machine
- Interpreter for a minimal G-machine
- Translation of minimal G-machine instructions into JVM byte code

As this code forms part of the [littlelanguages](https://little-languages.gitlab.io/overview) family, the implementation of `LIRO` and the G-machine follows the [littlelanguages](https://little-languages.gitlab.io/overview) style of [definition](https://little-languages.gitlab.io/overview/lir0/overview.html).

