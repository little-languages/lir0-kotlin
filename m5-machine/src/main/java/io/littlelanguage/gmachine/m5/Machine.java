package io.littlelanguage.gmachine.m5;

import java.util.ArrayList;
import java.util.List;

public class Machine {
    private final Node[] stack;
    private final List<Integer> bosses = new ArrayList<>();
    private int bos;
    private int tos;
    private int steps;

    public Machine(int stackSize) {
        stack = new Node[stackSize];
        bos = 0;
        tos = -1;
        steps = 0;
    }

    public void printStack(String instruction) {
        System.out.println("------------------------------------- " + steps);

        for (Integer boss : bosses) {
            System.out.print(">> ");
            System.out.println(boss);
        }
        for (int lp = 0; lp <= tos; lp += 1) {
            System.out.print("-");
            if (lp == bos) {
                System.out.print("*");
            } else if (bosses.contains(lp)) {
                System.out.print(">");
            } else {
                System.out.print(" ");
            }

            System.out.println(stack[lp].toString());
        }

        if (instruction != null) {
            System.out.println(instruction);
        }
    }

    private Node popStack() {
        Node result = stack[tos];
        tos -= 1;
        return result;
    }

    private void pushStack(Node n) {
        tos += 1;
        stack[tos] = n;
    }

    private Node peek() {
        return stack[tos];
    }

    public void unwindNInd(Node n) {
        steps += 1;
        stack[tos] = n.indNode();
    }

    public void unwindNAp(NAp n) {
        steps += 1;
        pushStack(n.e1());
    }

    public void unwindNGlobal(NGlobal n) {
        steps += 1;

        int parameterCount = n.sc().numberOfParameters();

        if (bos + parameterCount <= tos) {
            int stackPointer = tos;

            while (parameterCount > 0) {
                stack[stackPointer] = ((NAp) stack[stackPointer - 1]).e2();
                parameterCount -= 1;
                stackPointer -= 1;
            }

            n.sc().run(this);
        } else {
            tos = bos;
            bos = bosses.get(bosses.size() - 1);
            bosses.remove(bosses.size() - 1);
        }
    }

    public void unwindNNum(NNum n) {
        steps += 1;
    }

    public void unwind() {
        while (true) {
            Node n = peek();

            if (n.isInd()) {
                unwindNInd(n);
            } else if (n instanceof NAp) {
                unwindNAp((NAp) n);
            } else if (n instanceof NGlobal) {
                unwindNGlobal((NGlobal) n);
                break;
            } else if (bos == tos) {
                unwindNNum((NNum) n);
                if (!bosses.isEmpty()) {
                    bos = bosses.get(bosses.size() - 1);
                    bosses.remove(bosses.size() - 1);
                }
                break;
            } else {
                unwindNNum((NNum) n);
                break;
            }
        }
    }

    public void pushGlobal(SC sc) {
        steps += 1;
        pushStack(new NGlobal(sc));
    }

    public void pushInt(int n) {
        steps += 1;
        pushStack(new NNum(n));
    }

    public void push(int n) {
        steps += 1;
        pushStack(stack[tos - n]);
    }

    public void mkAp() {
        steps += 1;
        Node e1 = stack[tos];
        tos -= 1;
        Node e2 = stack[tos];
        stack[tos] = new NAp(e1, e2);
    }

    public void update(int n) {
        steps += 1;
        stack[tos - n - 1].mkInd(popStack());
    }

    public void pop(int n) {
        steps += 1;
        tos = tos - n;
    }

    public void slide(int n) {
        steps += 1;

        if (n > 0) {
            int newTos = tos - n;

            stack[newTos] = stack[tos];
            tos = newTos;
        }
    }

    public void alloc(int n) {
        steps += 1;

        while (n > 0) {
            pushStack(new NNum(n));
            n -= 1;
        }
    }

    public void eval() {
        steps += 1;

        bosses.add(bos);
        bos = tos;
        unwind();
    }

    public void add() {
        steps += 1;

        stack[tos - 1] = new NNum(unboxInt(tos) + unboxInt(tos - 1));
        tos -= 1;
    }

    public void sub() {
        steps += 1;

        stack[tos - 1] = new NNum(unboxInt(tos) - unboxInt(tos - 1));
        tos -= 1;
    }

    public void mul() {
        steps += 1;

        stack[tos - 1] = new NNum(unboxInt(tos) * unboxInt(tos - 1));
        tos -= 1;
    }

    public void div() {
        steps += 1;

        stack[tos - 1] = new NNum(unboxInt(tos) / unboxInt(tos - 1));
        tos -= 1;
    }

    public void neg() {
        steps += 1;

        stack[tos] = new NNum(-unboxInt(tos));
    }

    public void eq() {
        steps += 1;

        stack[tos - 1] = new NNum(boolAsInt(unboxInt(tos) == unboxInt(tos - 1)));
        tos -= 1;
    }

    public void ne() {
        steps += 1;

        stack[tos - 1] = new NNum(boolAsInt(unboxInt(tos) != unboxInt(tos - 1)));
        tos -= 1;
    }

    public void lt() {
        steps += 1;

        stack[tos - 1] = new NNum(boolAsInt(unboxInt(tos) < unboxInt(tos - 1)));
        tos -= 1;
    }

    public void le() {
        steps += 1;

        stack[tos - 1] = new NNum(boolAsInt(unboxInt(tos) <= unboxInt(tos - 1)));
        tos -= 1;
    }

    public void gt() {
        steps += 1;

        stack[tos - 1] = new NNum(boolAsInt(unboxInt(tos) > unboxInt(tos - 1)));
        tos -= 1;
    }

    public void ge() {
        steps += 1;

        stack[tos - 1] = new NNum(boolAsInt(unboxInt(tos) >= unboxInt(tos - 1)));
        tos -= 1;
    }

    public void cond(SC i1, SC i2) {
        steps += 1;

        boolean c = intAsBool(unboxInt(tos));
        tos -= 1;
        if (c)
            i1.run(this);
        else
            i2.run(this);
    }

    public boolean cond() {
        steps += 1;

        boolean c = intAsBool(unboxInt(tos));
        tos -= 1;

        return c;
    }

    public Node run(SC sc) {
        pushGlobal(sc);
        eval();

        return peek();
    }

    public int getSteps() {
        return steps;
    }

    private int unboxInt(int idx) {
        return ((NNum) stack[idx]).n();
    }

    private int boolAsInt(boolean v) {
        return v ? 1 : 0;
    }

    private boolean intAsBool(int v) {
        return v == 1;
    }
}
