package io.littlelanguage.gmachine.m5;

public class NGlobal extends Node {
    private Object sc;

    public NGlobal(SC sc) {
        this.sc = sc;
    }

    @Override
    public String show(int depth) {
        if (depth == 0)
            return "...";
        else if (sc instanceof Node)
            return "NInd(e=" + ((Node) sc).show(depth - 1) + ")";
        else
            return "NGlobal(sc=" + sc.toString() + ")";
    }

    public SC sc() {
        if (sc instanceof Node)
            throw new IllegalStateException("Attempt to get sc from NInd: " + toString());
        else
            return (SC) sc;
    }

    @Override
    public boolean isInd() {
        return sc instanceof Node;
    }

    @Override
    public Node indNode() {
        if (sc instanceof SC)
            throw new IllegalStateException("Attempt to get ind from NGlobal: " + toString());
        else
            return (Node) sc;
    }

    @Override
    public void mkInd(Node node) {
        sc = node;
    }
}
