package io.littlelanguage.gmachine.m5;

public interface SC {
    void run(Machine machine);

    int numberOfParameters();
}
