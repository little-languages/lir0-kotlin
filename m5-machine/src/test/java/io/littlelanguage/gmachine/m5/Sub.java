package io.littlelanguage.gmachine.m5;

class Sub implements SC {
    public static final Sub INSTANCE = new Sub();

    @Override
    public void run(Machine m) {
        m.push(1);
        m.eval();
        m.push(1);
        m.eval();
        m.sub();
        m.update(2);
        m.pop(2);
        m.unwind();
    }

    @Override
    public int numberOfParameters() {
        return 2;
    }

    @Override
    public String toString() {
        return "Sub";
    }
}
