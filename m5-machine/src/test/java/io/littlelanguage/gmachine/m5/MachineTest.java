package io.littlelanguage.gmachine.m5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MachineTest {
    @Test
    public void testThoughts() {
        Node n =
                new FMachine(100).run(Main.INSTANCE);

        while (n.isInd()) {
            n = n.indNode();
        }

        assertTrue(n instanceof NNum);
        assertEquals(((NNum) n).n(), 3);
    }

    @Test
    public void testFact() {
        Node n =
                new FMachine(100).run(FacMain.INSTANCE);

        while (n.isInd()) {
            n = n.indNode();
        }

        assertTrue(n instanceof NNum);
        assertEquals(((NNum) n).n(), 120);
    }
}

