package io.littlelanguage.gmachine.m5;

class Id implements SC {
    public static final Id INSTANCE = new Id();

    @Override
    public void run(Machine m) {
        m.pushGlobal(K.INSTANCE);
        m.pushGlobal(K.INSTANCE);
        m.pushGlobal(S.INSTANCE);
        m.mkAp();
        m.mkAp();
        m.update(0);
        m.pop(0);
        m.unwind();
    }

    @Override
    public int numberOfParameters() {
        return 0;
    }

    @Override
    public String toString() {
        return "Id";
    }
}
