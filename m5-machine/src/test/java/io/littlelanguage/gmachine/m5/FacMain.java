package io.littlelanguage.gmachine.m5;

class FacMain implements SC {
    public static final FacMain INSTANCE = new FacMain();

    @Override
    public void run(Machine m) {
        m.pushInt(5);
        m.pushGlobal(Fac.INSTANCE);
        m.mkAp();
        m.eval();
        m.update(0);
        m.pop(0);
        m.unwind();
    }

    @Override
    public int numberOfParameters() {
        return 0;
    }

    @Override
    public String toString() {
        return "FacMain";
    }


    public static void main(String[] args) {
        System.out.println(new FMachine(100).run(FacMain.INSTANCE));
    }
}
