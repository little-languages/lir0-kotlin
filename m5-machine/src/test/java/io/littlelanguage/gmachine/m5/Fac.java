package io.littlelanguage.gmachine.m5;

class Fac implements SC {
    public static final Fac INSTANCE = new Fac();

    @Override
    public void run(Machine m) {
        m.pushInt(0);
        m.push(1);
        m.eval();
        m.eq();

        if (m.cond()) {
            m.pushInt(1);
        } else {
            m.pushInt(1);
            m.push(1);
            m.pushGlobal(Sub.INSTANCE);
            m.mkAp();
            m.mkAp();
            m.pushGlobal(Fac.INSTANCE);
            m.mkAp();
            m.eval();
            m.push(1);
            m.eval();
            m.mul();
        }

        m.update(1);
        m.pop(1);
        m.unwind();
    }

    @Override
    public int numberOfParameters() {
        return 1;
    }

    @Override
    public String toString() {
        return "Fac";
    }
}
