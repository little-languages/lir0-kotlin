package io.littlelanguage.gmachine.m5;

class K implements SC {
    public static final K INSTANCE = new K();

    @Override
    public void run(Machine m) {
        m.push(0);
        m.update(2);
        m.pop(2);
    }

    @Override
    public int numberOfParameters() {
        return 2;
    }

    @Override
    public String toString() {
        return "K";
    }
}
