package io.littlelanguage.gmachine.m1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MachineTest {
    @Test
    public void testThoughts() {
        Node n =
                new Machine(100).run(Main.INSTANCE);

        assertTrue(n instanceof NNum);
        assertEquals(((NNum) n).n, 3);
    }
}

