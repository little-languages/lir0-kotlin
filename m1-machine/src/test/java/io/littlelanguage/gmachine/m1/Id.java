package io.littlelanguage.gmachine.m1;

class Id implements SC {
    public static final Id INSTANCE = new Id();

    @Override
    public void run(Machine m) {
        m.pushGlobal(K.INSTANCE);
        m.pushGlobal(K.INSTANCE);
        m.pushGlobal(S.INSTANCE);
        m.mkAp();
        m.mkAp();
        m.slide(1);
        m.unwind();
    }

    @Override
    public String toString() {
        return "Id";
    }
}
