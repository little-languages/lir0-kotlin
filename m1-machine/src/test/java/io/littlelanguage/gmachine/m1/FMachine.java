package io.littlelanguage.gmachine.m1;

class FMachine extends Machine {

    public FMachine(int stackSize) {
        super(stackSize);
    }

    @Override
    public void pushGlobal(SC sc) {
        printStack("pushGlobal " + sc.toString());
        super.pushGlobal(sc);
    }

    @Override
    public void pushInt(int n) {
        printStack("pushInt " + n);
        super.pushInt(n);
    }

    @Override
    public void mkAp() {
        printStack("mkAp");
        super.mkAp();
    }

    @Override
    public void push(int n) {
        printStack("push " + n);
        super.push(n);
    }

    @Override
    public void slide(int n) {
        printStack("slide " + n);
        super.slide(n);
    }

    @Override
    public void unwind() {
        printStack("unwind");
        super.unwind();
    }
}
