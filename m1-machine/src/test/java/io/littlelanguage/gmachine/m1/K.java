package io.littlelanguage.gmachine.m1;

class K implements SC {
    public static final K INSTANCE = new K();

    @Override
    public void run(Machine m) {
        m.push(0);
        m.slide(3);
    }

    @Override
    public String toString() {
        return "K";
    }
}
