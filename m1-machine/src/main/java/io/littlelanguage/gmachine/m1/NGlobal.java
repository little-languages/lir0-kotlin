package io.littlelanguage.gmachine.m1;

public class NGlobal extends Node {
    public final SC sc;

    public NGlobal(SC sc) {
        this.sc = sc;
    }

    public String toString() {
        return "NGlobal(sc=" + sc.toString() + ")";
    }
}
