package io.littlelanguage.gmachine.m1;

public class NNum extends Node {
    public final int n;

    public NNum(int n) {
        this.n = n;
    }

    public String toString() {
        return "NNum(n=" + Integer.toString(n) + ")";
    }
}
