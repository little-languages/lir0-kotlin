package io.littlelanguage.gmachine.m1;

public class NAp extends Node {
    public final Node e1;
    public final Node e2;

    public NAp(Node e1, Node e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    public String toString() {
        return "NAp(e1=" + e1.toString() + ", e2=" + e2.toString() + ")";
    }
}
