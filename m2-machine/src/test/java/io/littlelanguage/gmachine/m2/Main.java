package io.littlelanguage.gmachine.m2;

class Main implements SC {
    public static final Main INSTANCE = new Main();

    @Override
    public void run(Machine m) {
        m.pushInt(3);
        m.pushGlobal(Id.INSTANCE);
        m.mkAp();
        m.update(0);
        m.pop(0);
        m.unwind();
    }

    @Override
    public String toString() {
        return "Main";
    }


    public static void main(String[] args) {
        System.out.println(new FMachine(100).run(Main.INSTANCE));
    }
}
