package io.littlelanguage.gmachine.m2;

public class NNum extends Node {
    private int n;
    private Node ind;


    public NNum(int n) {
        this.n = n;
        ind = null;
    }

    public String toString() {
        return "NNum(n=" + n + ")";
    }

    public int n() {
        if (ind == null)
            return n;
        else
            throw new IllegalStateException("Attempt to get n from NInd: " + toString());
    }

    @Override
    public boolean isInd() {
        return ind != null;
    }

    @Override
    public Node indNode() {
        if (ind == null)
            throw new IllegalStateException("Attempt to get ind from NNum: " + toString());
        else
            return ind;
    }

    @Override
    public void mkInd(Node node) {
        ind = node;
    }
}
