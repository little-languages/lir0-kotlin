package io.littlelanguage.gmachine.m2;

public class Machine {
    private final Node[] stack;
    private int tos;
    private int steps;

    public Machine(int stackSize) {
        stack = new Node[stackSize];
        tos = -1;
        steps = 0;
    }

    public void printStack(String instruction) {
        System.out.println("-------------------------------------");

        for (int lp = 0; lp <= tos; lp += 1) {
            System.out.print(". ");
            System.out.println(stack[lp].toString());
        }

        if (instruction != null) {
            System.out.println(instruction);
        }
    }

    private Node popStack() {
        Node result = stack[tos];
        tos -= 1;
        return result;
    }

    private void pushStack(Node n) {
        tos += 1;
        stack[tos] = n;
    }

    private Node peek() {
        return stack[tos];
    }

    public void pushGlobal(SC sc) {
        steps += 1;
        pushStack(new NGlobal(sc));
    }

    public void pushInt(int n) {
        steps += 1;
        pushStack(new NNum(n));
    }

    public void push(int n) {
        steps += 1;
        pushStack(((NAp) stack[tos - n - 1]).e2());
    }

    public void mkAp() {
        steps += 1;
        Node e1 = stack[tos];
        tos -= 1;
        Node e2 = stack[tos];
        stack[tos] = new NAp(e1, e2);
    }

    public void update(int n) {
        steps += 1;
        stack[tos - n - 1].mkInd(popStack());
    }

    public void pop(int n) {
        steps += 1;
        tos = tos - n;
    }

    public void unwindNInd(Node n) {
        steps += 1;
        stack[tos] = n.indNode();
    }

    public void unwindNAp(NAp n) {
        steps += 1;
        pushStack(n.e1());
    }

    public void unwindNGlobal(NGlobal n) {
        steps += 1;
        n.sc().run(this);
    }

    public void unwindNNum(NNum n) {
        steps += 1;
    }

    public void unwind() {
        while (true) {
            Node n = peek();

            if (n.isInd()) {
                unwindNInd(n);
            } else if (n instanceof NAp) {
                unwindNAp((NAp) n);
            } else if (n instanceof NGlobal) {
                unwindNGlobal((NGlobal) n);
                break;
            } else {
                unwindNNum((NNum) n);
                break;
            }
        }
    }

    public Node run(SC sc) {
        pushGlobal(sc);
        unwind();

        return peek();
    }

    public int getSteps() {
        return steps;
    }
}
