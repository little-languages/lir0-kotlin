package io.littlelanguage.gmachine.m2;

public abstract class Node {
    abstract public boolean isInd();
    abstract public Node indNode();
    abstract public void mkInd(Node node);
}
