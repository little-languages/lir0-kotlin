package io.littlelanguage.lir0.dynamic

import io.littlelanguage.lir0.dynamic.ast.*

fun translate(p: io.littlelanguage.lir0.static.ast.Program): Program =
        Translator().p(p)

private class Translator {
    fun p(p: io.littlelanguage.lir0.static.ast.Program): Program =
            p.scdefns.map { sc(it) }

    private fun sc(scDef: io.littlelanguage.lir0.static.ast.SCDefn): SCDefn =
            Triple(n(scDef.name), scDef.parameters.map { n(it) }, e(scDef.expr))

    private fun n(n: io.littlelanguage.lir0.static.ast.Name): Name =
            n.value

    private fun e(e: io.littlelanguage.lir0.static.ast.Expr): Expr =
            when (e) {
                is io.littlelanguage.lir0.static.ast.EVar ->
                    EVar(e.name)
                is io.littlelanguage.lir0.static.ast.ENum ->
                    ENum(e.value.value.toInt())
                is io.littlelanguage.lir0.static.ast.EConstr ->
                    EConstr(e.arity.value.toInt(), e.tag.value.toInt())
                is io.littlelanguage.lir0.static.ast.EParen ->
                    e(e.expr)
                is io.littlelanguage.lir0.static.ast.EAp ->
                    EAp(e(e.e1), e(e.e2))
                is io.littlelanguage.lir0.static.ast.ELet ->
                    ELet(
                            if (e.isRec == io.littlelanguage.lir0.static.ast.IsRec.NonRecursive) IsRec.NonRecursive else IsRec.Recursive,
                            e.decls.map { Pair(n(it.first), e(it.second)) },
                            e(e.expr)
                    )
                is io.littlelanguage.lir0.static.ast.ECase ->
                    ECase(e(e.expr), e.alters.map {
                        Triple(it.value.value.toInt(), it.names.map { n -> n(n) }, e(it.expr))
                    })
                is io.littlelanguage.lir0.static.ast.ELam ->
                    ELam(e.names.map { n(it) }, e(e.expr))
                else -> TODO(e.toString())
            }
}