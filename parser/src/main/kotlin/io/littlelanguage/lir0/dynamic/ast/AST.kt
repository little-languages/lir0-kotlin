package io.littlelanguage.lir0.dynamic.ast

import io.littlelanguage.text.prettyprint.*


typealias Program =
        List<SCDefn>

typealias SCDefn =
        Triple<Name, List<Name>, Expr>

typealias Name =
        String

open class Expr

data class EVar(
        val name: String) : Expr()

data class ENum(
        val value: Int) : Expr()

data class EConstr(
        val tag: Int,
        val arity: Int) : Expr()

data class EAp(
        val e1: Expr,
        val e2: Expr) : Expr()

data class ELet(
        val isRec: IsRec,
        val decls: List<Pair<Name, Expr>>,
        val expr: Expr) : Expr()

data class ECase(
        val expr: Expr,
        val alters: List<Alter>) : Expr()

data class ELam(
        val names: List<Name>,
        val expr: Expr) : Expr()

enum class IsRec {
    Recursive, NonRecursive
}

typealias Alter =
        Triple<Int, List<Name>, Expr>

fun ppProgram(p: Program): Doc =
        vcat(punctuate(text(";"), p.map { ppSCDefn(it) }))

fun ppSCDefn(scDefn: SCDefn): Doc =
        text(scDefn.first) pp hsep(scDefn.second.map { text(it) }) pp text("=") pp ppExpr(scDefn.third)

fun ppExpr(e: Expr, p: Int = -1): Doc =
        when (e) {
            is EVar -> text(e.name)
            is ENum -> int(e.value)
            is EConstr -> text("Pack{") + int(e.tag) + text(", ") + int(e.arity) + text(")")
            is EAp -> paren(p, 6, ppExpr(e.e1, 6) pp ppExpr(e.e2, 6))
            is ELet ->
                if (e.isRec == IsRec.NonRecursive)
                    vcat(listOf(
                            text("let") pp nest(0, vcat(punctuate(text(";"), e.decls.map { text(it.first) pp text("=") pp ppExpr(it.second) }))),
                            text("in") pp ppExpr(e.expr)
                    ))
                else
                    vcat(listOf(
                            text("letrec") pp nest(0, vcat(punctuate(text(";"), e.decls.map { text(it.first) pp text("=") pp ppExpr(it.second) }))),
                            text("in") pp ppExpr(e.expr)
                    ))
            is ECase ->
                paren(p, 0,
                        vcat(listOf(
                                text("case") pp ppExpr(e.expr) pp text("of"),
                                nest(2, vcat(e.alters.map { text("<") + int(it.first) + text(">") pp hsep(it.second.map { n -> text(n) }) pp text("->") pp ppExpr(it.third) }))
                        ))
                )
            is ELam -> paren(p, 0, text("\\") + hsep(e.names.map { text(it) }) pp text(".") pp ppExpr(e.expr))
            else -> text("...$e...")
        }

private fun paren(fp: Int, tp: Int, d: Doc): Doc =
        if (fp >= tp) text("(") + d + text(")") else d
