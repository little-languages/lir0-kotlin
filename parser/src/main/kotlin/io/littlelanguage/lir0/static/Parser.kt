package io.littlelanguage.lir0.static

import io.littlelanguage.data.Either
import io.littlelanguage.data.Left
import io.littlelanguage.data.Right
import io.littlelanguage.lir0.Errors
import io.littlelanguage.lir0.ParseError
import io.littlelanguage.lir0.lexer.LA
import io.littlelanguage.lir0.lexer.TToken
import io.littlelanguage.lir0.lexer.Token
import io.littlelanguage.lir0.static.ast.*

fun parse(la: LA): Either<Errors, Program> =
        Parser(la).program()

class Parser(private val la: LA) {
    fun program(): Either<Errors, Program> =
            try {
                val scDefns =
                        mutableListOf<SCDefn>()

                scDefns.add(sc())
                while (peek().tToken == TToken.TSemicolon) {
                    skipToken()
                    scDefns.add(sc())
                }
                matchToken(TToken.TEOS)

                Right(Program(scDefns))
            } catch (e: ParsingException) {
                Left(ParseError(e.found, e.expected))
            }

    fun sc(): SCDefn {
        val identifier =
                name()

        val names =
                mutableListOf<Name>()

        while (peek().tToken == TToken.TIdentifier) {
            names.add(name())
        }

        matchToken(TToken.TEqual)

        val expr =
                expression()

        return SCDefn(identifier.location() + expr.location(), identifier, names, expr)
    }

    private fun expression(): Expr {
        val result =
                andExpression()

        return if (peek().tToken == TToken.TBar) {
            val bar =
                    nextToken()

            val right =
                    expression()

            EAp(EAp(EVar(bar.location, bar.lexeme), result), right)
        } else
            result;
    }

    private fun andExpression(): Expr {
        val result =
                relationalExpression()

        return if (peek().tToken == TToken.TAmpersand) {
            val ampersand =
                    nextToken()

            val right =
                    andExpression()

            EAp(EAp(EVar(ampersand.location, ampersand.lexeme), result), right)
        } else
            result;
    }

    private fun relationalExpression(): Expr {
        val result =
                additiveExpression()

        return if (relationalOperators.contains(peek().tToken)) {
            val operator =
                    nextToken()

            val right =
                    additiveExpression()

            EAp(EAp(EVar(operator.location, operator.lexeme), result), right)
        } else
            result;
    }

    private val relationalOperators =
            setOf(TToken.TEqualEqual, TToken.TTildeEqual, TToken.TLessEqual, TToken.TLessThan, TToken.TGreaterEqual, TToken.TGreaterThan)

    private fun additiveExpression(): Expr {
        val result =
                multiplicativeExpression()

        return if (additiveOperators.contains(peek().tToken)) {
            val operator =
                    nextToken()

            val right =
                    additiveExpression()

            EAp(EAp(EVar(operator.location, operator.lexeme), result), right)
        } else
            result;
    }

    private val additiveOperators =
            setOf(TToken.TPlus, TToken.TMinus)

    private fun multiplicativeExpression(): Expr {
        val result =
                applicativeExpression()

        return if (multiplicativeOperators.contains(peek().tToken)) {
            val operator =
                    nextToken()

            val right =
                    multiplicativeExpression()

            EAp(EAp(EVar(operator.location, operator.lexeme), result), right)
        } else
            result;
    }

    private val multiplicativeOperators =
            setOf(TToken.TStar, TToken.TSlash)

    private fun applicativeExpression(): Expr {
        var result =
                factor()

        while (firstFactor.contains(peek().tToken)) {
            result = EAp(result, factor())
        }

        return result
    }

    private fun factor(): Expr =
            when (peek().tToken) {
                TToken.TLet, TToken.TLetrec -> {
                    val token =
                            nextToken()

                    val isRec =
                            if (token.tToken == TToken.TLet) IsRec.NonRecursive else IsRec.Recursive

                    val defns =
                            defns()

                    matchToken(TToken.TIn)

                    val expr =
                            expression()

                    ELet(token.location + expr.location(), isRec, defns, expr)
                }
                TToken.TCase -> {
                    val token =
                            nextToken()

                    val expr =
                            expression()

                    matchToken(TToken.TOf)

                    val alts =
                            alts()

                    ECase(token.location + alts.last().location(), expr, alts)
                }
                TToken.TBackslash -> {
                    val token =
                            nextToken()

                    val names =
                            mutableListOf(name())

                    while (peek().tToken == TToken.TIdentifier) {
                        names.add(name())
                    }

                    matchToken(TToken.TPeriod)

                    val expr =
                            expression()

                    ELam(token.location + expr.location(), names, expr)
                }
                TToken.TIdentifier -> {
                    val token =
                            nextToken()

                    EVar(token.location, token.lexeme)
                }
                TToken.TLiteralInt -> {
                    val token =
                            nextToken()

                    ENum(ConstantInt(token.location, token.lexeme))
                }
                TToken.TPack -> {
                    val token =
                            nextToken()

                    matchToken(TToken.TLCurly)

                    val tag =
                            matchToken(TToken.TLiteralInt)

                    matchToken(TToken.TComma)

                    val arity =
                            matchToken(TToken.TLiteralInt)

                    val rcurly =
                            matchToken(TToken.TRCurly)

                    EConstr(token.location + rcurly.location, ConstantInt(tag.location, tag.lexeme), ConstantInt(arity.location, arity.lexeme))
                }
                TToken.TLParen -> {
                    val lparen =
                            nextToken()

                    val expr =
                            expression()

                    val rparen =
                            matchToken(TToken.TRParen)

                    EParen(lparen.location + rparen.location, expr)
                }

                else -> throw ParsingException(peek(), firstFactor)
            }


    private fun defns(): List<Pair<Name, Expr>> {
        val defns =
                mutableListOf(defn())

        while (peek().tToken == TToken.TSemicolon) {
            skipToken()

            defns.add(defn())
        }

        return defns
    }

    private fun defn(): Pair<Name, Expr> {
        val name =
                name()

        matchToken(TToken.TEqual)

        val expr =
                expression()

        return Pair(name, expr)
    }


    private fun alts(): List<Alter> {
        val defns =
                mutableListOf(alt())

        while (peek().tToken == TToken.TSemicolon) {
            skipToken()

            defns.add(alt())
        }

        return defns
    }

    private fun alt(): Alter {
        val name =
                matchToken(TToken.TLessThan)

        val value =
                matchToken(TToken.TLiteralInt)

        matchToken(TToken.TGreaterThan)

        val names =
                mutableListOf<Name>()

        while (peek().tToken == TToken.TIdentifier) {
            names.add(name())
        }

        matchToken(TToken.TMinusGreaterThan)

        val expr =
                expression()

        return Alter(name.location + expr.location(), ConstantInt(value.location, value.lexeme), names, expr)
    }


    private fun name(): Name {
        val token =
                matchToken(TToken.TIdentifier)

        return Name(token.location, token.lexeme)
    }

    private fun matchToken(tToken: TToken): Token =
            when (peek().tToken) {
                tToken -> nextToken()
                else -> throw ParsingException(peek(), setOf(tToken))
            }

    private fun nextToken(): Token {
        val result =
                peek()

        skipToken()

        return result
    }

    private fun skipToken() {
        la.next()
    }

    private fun peek(): Token =
            la.current
}

private val firstFactor = setOf(TToken.TLet, TToken.TLetrec, TToken.TCase, TToken.TBackslash, TToken.TIdentifier, TToken.TLiteralInt, TToken.TPack, TToken.TLParen)

class ParsingException(
        val found: Token,
        val expected: Set<TToken>) : Exception()