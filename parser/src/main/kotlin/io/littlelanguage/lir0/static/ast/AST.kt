package io.littlelanguage.lir0.static.ast

import io.littlelanguage.data.Yamlable
import io.littlelanguage.lir0.Location
import io.littlelanguage.lir0.Locationable

data class Program(
        val scdefns: List<SCDefn>) : Yamlable {
    override fun yaml(): Any =
            scdefns.map { it.yaml() }
}

data class SCDefn(
        val location: Location,
        val name: Name,
        val parameters: List<Name>,
        val expr: Expr) : Locationable, Yamlable {
    override fun location(): Location = location

    override fun yaml(): Any =
            mapOf(
                    Pair("location", location.yaml()),
                    Pair("ns", parameters.map { it.yaml() }),
                    Pair("n", name.yaml()),
                    Pair("e", expr.yaml())
            )
}

data class Name(
        val location: Location,
        val value: String) : Locationable, Yamlable {
    override fun location(): Location = location

    override fun yaml(): Any =
            mapOf(
                    Pair("location", location.yaml()),
                    Pair("value", value)
            )
}

abstract class Expr : Locationable, Yamlable

data class EVar(
        val location: Location,
        val name: String) : Expr() {
    override fun location(): Location = location

    override fun yaml(): Any =
            singletonMap("EVar", mapOf(
                    Pair("location", location.yaml()),
                    Pair("value", name)
            ))
}

data class ENum(
        val value: ConstantInt) : Expr() {
    override fun location(): Location = value.location

    override fun yaml(): Any =
            singletonMap("ENum", value.yaml())
}

data class EConstr(
        val location: Location,
        val tag: ConstantInt,
        val arity: ConstantInt) : Expr() {
    override fun location(): Location = location

    override fun yaml(): Any =
            singletonMap("EConstr", mapOf(
                    Pair("arity", arity.yaml()),
                    Pair("tag", tag.yaml()),
                    Pair("location", location.yaml())
            ))
}

data class EParen(
        val location: Location,
        val expr: Expr) : Expr() {
    override fun location(): Location = location

    override fun yaml(): Any =
            singletonMap("EParen", mapOf(
                    Pair("location", location.yaml()),
                    Pair("e", expr.yaml())
            ))
}

data class EAp(
        val e1: Expr,
        val e2: Expr) : Expr() {
    override fun location(): Location = e1.location() + e2.location()

    override fun yaml(): Any =
            singletonMap("EAp", mapOf(
                    Pair("e1", e1.yaml()),
                    Pair("e2", e2.yaml())
            ))
}

data class ELet(
        val location: Location,
        val isRec: IsRec,
        val decls: List<Pair<Name, Expr>>,
        val expr: Expr) : Expr() {
    override fun location(): Location = location

    override fun yaml(): Any =
            singletonMap("ELet", mapOf(
                    Pair("location", location.yaml()),
                    Pair("isRec", isRec.yaml()),
                    Pair("e", expr.yaml()),
                    Pair("ps", decls.map { mapOf(Pair("n", it.first.yaml()), Pair("e", it.second.yaml())) })
            ))
}

data class ECase(
        val location: Location,
        val expr: Expr,
        val alters: List<Alter>) : Expr() {
    override fun location(): Location = location

    override fun yaml(): Any =
            singletonMap("ECase", mapOf(
                    Pair("as", alters.map { it.yaml() }),
                    Pair("location", location.yaml()),
                    Pair("e", expr.yaml())
            ))
}

data class ELam(
        val location: Location,
        val names: List<Name>,
        val expr: Expr) : Expr() {
    override fun location(): Location = location

    override fun yaml(): Any =
            singletonMap("ELam", mapOf(
                    Pair("location", location.yaml()),
                    Pair("ns", names.map { it.yaml() }),
                    Pair("e", expr.yaml())
            ))
}

enum class IsRec : Yamlable {
    Recursive {
        override fun yaml(): Any = "Recursive"
    },
    NonRecursive {
        override fun yaml(): Any = "NonRecursive"
    }
}

data class Alter(
        val location: Location,
        val value: ConstantInt,
        val names: List<Name>,
        val expr: Expr) : Locationable, Yamlable {
    override fun location(): Location = location

    override fun yaml(): Any =
            mapOf(
                    Pair("location", location.yaml()),
                    Pair("ns", names.map { it.yaml() }),
                    Pair("e", expr.yaml()),
                    Pair("c", value.yaml())
            )
}

data class ConstantInt(
        val location: Location,
        val value: String) : Locationable, Yamlable {
    override fun location(): Location = location

    override fun yaml(): Any =
            mapOf(
                    Pair("location", location.yaml()),
                    Pair("value", value)
            )
}