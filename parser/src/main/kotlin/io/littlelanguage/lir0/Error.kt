package io.littlelanguage.lir0

import io.littlelanguage.data.Yamlable
import io.littlelanguage.lir0.lexer.TToken
import io.littlelanguage.lir0.lexer.Token

sealed class Errors : Yamlable

data class ParseError(
        val found: Token,
        val expected: Set<TToken>) : Errors() {
    override fun yaml(): Any =
            singletonMap("ParseError", mapOf(
                    Pair("found", found),
                    Pair("expected", expected)
            ))
}
