package io.littlelanguage.lir0.lexer

import io.littlelanguage.lir0.Location
import io.littlelanguage.lir0.LocationCoordinate
import io.littlelanguage.lir0.LocationRange

enum class TToken {
    TEOS, TERROR,

    TSingleLineComment, TMultiLineComment,

    TCase, TIn, TLet, TLetrec, TOf, TPack,

    TAmpersand, TBackslash, TBar, TComma, TEqual, TEqualEqual, TGreaterEqual, TGreaterThan, TLCurly, TLessEqual, TLessThan,
    TLParen, TMinus, TMinusGreaterThan, TPeriod, TPlus, TRCurly, TRParen, TSemicolon, TSlash, TStar, TTildeEqual,

    TIdentifier, TLiteralInt
}


data class Token(val tToken: TToken, val location: Location, val lexeme: String) {
    override fun toString(): String {
        fun pp(location: Location): String =
                when (location) {
                    is LocationCoordinate -> location.offset.toString()
                    is LocationRange ->
                        if (location.start == location.end)
                            pp(location.start)
                        else
                            pp(location.start) + "-" + pp(location.end)
                }

        return tToken.toString().drop(1) + " " + pp(location) + " [" + lexeme + "]"
    }
}
