package io.littlelanguage.text.prettyprint

import java.lang.Integer.max

open class Doc {
    operator fun plus(other: Doc): Doc =
            PlusDoc(this, other)
}

private object EmptyDoc : Doc()

private class VerticalDoc(val docs: List<Doc>) : Doc()

private class TextDoc(val text: String) : Doc()

private class PlusDoc(val l: Doc, val r: Doc) : Doc()

private class PlusPlusDoc(val l: Doc, val r: Doc) : Doc()

private class NestDoc(val offset: Int, val doc: Doc) : Doc()


val empty: Doc =
        EmptyDoc

val space =
        text(" ")

fun text(t: String): Doc =
        TextDoc(t)

fun int(v: Int): Doc =
        TextDoc(v.toString())

infix fun Doc.pp(o: Doc): Doc =
        PlusPlusDoc(this, o)

fun vcat(docs: List<Doc>): Doc =
        VerticalDoc(docs)

fun hsep(docs: List<Doc>): Doc =
        when (docs.size) {
            0 ->
                empty

            1 ->
                docs[0]

            else ->
                docs.drop(1).fold(docs[0], { acc, new -> acc pp new })
        }

fun nest(offset: Int, doc: Doc): Doc =
        NestDoc(offset, doc)

fun punctuate(separator: Doc, docs: List<Doc>): List<Doc> {
    val last =
            docs.size

    val result =
            mutableListOf<Doc>()

    return if (last == 0 || last == 1)
        docs
    else {
        for (lp in 0..last - 2)
            result.add(docs[lp] + separator)

        result.add(docs[last - 1])

        result
    }
}

fun render(doc: Doc): String {
    val sb = StringBuffer()
    render(doc, 0, 0, sb)
    return sb.toString()
}

private fun render(doc: Doc, leftMargin: Int, offset: Int, sb: StringBuffer): Int =
        when (doc) {
            is EmptyDoc ->
                offset

            is VerticalDoc -> {
                if (doc.docs.isEmpty())
                    offset
                else {
                    val lm =
                            max(leftMargin, offset)

                    var o =
                            offset
                    while (o < lm) {
                        sb.append(' ')
                        o += 1
                    }

                    render(doc.docs[0], lm, o, sb)
                    sb.append("\n")
                    o = 0

                    doc.docs.drop(1).forEachIndexed { idx, line ->
                        while (o < lm) {
                            sb.append(' ')
                            o += 1
                        }
                        o = render(line, lm, o, sb)

                        if (idx != doc.docs.size - 2) {
                            sb.append("\n")
                            o = 0
                        }
                    }

                    o
                }
            }
            is TextDoc -> {
                sb.append(doc.text)
                offset + doc.text.length
            }
            is PlusDoc -> {
                val l =
                        render(doc.l, leftMargin, offset, sb)

                render(doc.r, leftMargin, l, sb)
            }
            is PlusPlusDoc -> {
                if (doc.l == empty && doc.r == empty)
                    offset
                else if (doc.l == empty)
                    render(doc.r, leftMargin, offset, sb)
                else if (doc.r == empty)
                    render(doc.l, leftMargin, offset, sb)
                else {
                    val l =
                            render(doc.l, leftMargin, offset, sb)

                    sb.append(' ')

                    render(doc.r, leftMargin, l + 1, sb)
                }
            }

            is NestDoc ->
                render(doc.doc, offset + doc.offset, offset, sb)

            else -> TODO(doc.toString())
        }
