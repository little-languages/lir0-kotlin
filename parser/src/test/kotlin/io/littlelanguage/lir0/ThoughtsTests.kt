package io.littlelanguage.lir0

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe


class ThoughtsTests : StringSpec({
    "id = S K K ; main = id 3" {
        M1Machine(true).run(Main) shouldBe NNum(3)
    }
})


class M1Machine(private val verbose: Boolean = true) {
    private val stack =
            mutableListOf<Node>()

    private fun printStack(instruction: String? = null) {
        if (verbose) {
            println("-------------------------------------")
            stack.forEach {
                println(". $it")
            }
            if (instruction != null) {
                println(instruction)
            }
        }
    }

    private fun popStack(): Node {
        val r = stack[stack.size - 1]
        stack.removeAt(stack.size - 1)
        return r
    }

    private fun pushStack(n: Node) {
        stack.add(n)
    }

    private fun peek(i: Int = 0): Node =
            stack[stack.size - i - 1]

    fun pushGlobal(sc: SC) {
        printStack("pushGlobal $sc")
        pushStack(NGlobal(sc))
    }

    fun pushInt(n: Int) {
        printStack("pushInt $n")
        pushStack(NNum(n))
    }

    fun mkAp() {
        printStack("mkAp")
        val e1 = popStack()
        val e2 = popStack()
        pushStack(NAp(e1, e2))
    }

    fun push(n: Int) {
        printStack("push $n")

        when (val e = peek(n + 1)) {
            is NAp -> {
                pushStack(e.e2)
            }
            else -> throw RuntimeException("Push value is not an NAp: $e")
        }
    }

    fun slide(n: Int) {
        printStack("slide $n")
        val v = popStack()

        for (l in 1..n) {
            popStack()
        }
        pushStack(v)
    }

    fun unwind() {
        var loop =
                true

        while (loop) {
            printStack("unwind")

            when (val v = peek()) {
                is NNum -> loop = false
                is NAp -> pushStack(v.e1)
                is NGlobal -> v.sc.run(this)
            }
        }
    }

    fun run(sc: SC): Node {
        pushGlobal(sc)
        unwind()

        return peek()
    }
}

abstract class SC {
    abstract fun run(m: M1Machine)
}

open class Node

data class NNum(val n: Int) : Node()
data class NAp(val e1: Node, val e2: Node) : Node()
data class NGlobal(val sc: SC) : Node()

object K : SC() {
    override fun run(m: M1Machine) {
        m.push(0)
        m.slide(3)
    }

    override fun toString(): String = "K"
}

object S : SC() {
    override fun run(m: M1Machine) {
        m.push(2)
        m.push(2)
        m.mkAp()
        m.push(3)
        m.push(2)
        m.mkAp()
        m.mkAp()
        m.slide(4)
        m.unwind()
    }

    override fun toString(): String = "S"
}

object Id : SC() {
    override fun run(m: M1Machine) {
        m.pushGlobal(K)
        m.pushGlobal(K)
        m.pushGlobal(S)
        m.mkAp()
        m.mkAp()
        m.slide(1)
        m.unwind()
    }

    override fun toString(): String = "Id"
}

object Main : SC() {
    override fun run(m: M1Machine) {
        m.pushInt(3)
        m.pushGlobal(Id)
        m.mkAp()
        m.slide(1)
        m.unwind()
    }

    override fun toString(): String = "Main"
}
