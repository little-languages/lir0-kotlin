package io.littlelanguage.lir0.semantic.gm4

import io.littlelanguage.data.Either
import io.littlelanguage.data.Right
import io.littlelanguage.lir0.Errors
import io.littlelanguage.lir0.dynamic.ast.EAp
import io.littlelanguage.lir0.dynamic.ast.EVar
import io.littlelanguage.lir0.dynamic.ast.Program
import io.littlelanguage.lir0.dynamic.ast.SCDefn
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

typealias CompileOutput =
        List<Pair<String, ByteArray>>

fun compile(p: Program, packageName: String): Either<List<Errors>, CompileOutput> =
        Right(compileP(p, packageName))

fun compileP(p: Program, packageName: String): CompileOutput {
    val preludeDefs =
            listOf(
                    Triple("I", listOf("x"), EVar("x")),
                    Triple("K", listOf("x", "y"), EVar("x")),
                    Triple("K1", listOf("x", "y"), EVar("y")),
                    Triple("S", listOf("f", "g", "x"), EAp(EAp(EVar("f"), EVar("x")), EAp(EVar("g"), EVar("x")))),
                    Triple("compose", listOf("f", "g", "x"), EAp(EVar("f"), EAp(EVar("g"), EVar("x")))),
                    Triple("twice", listOf("f"), EAp(EAp(EVar("compose"), EVar("f")), EVar("f")))
            )

    return (p + preludeDefs).map { compileSC(it, packageName) } + compiledPrimitives(packageName)
}

private fun compileSC(scDefn: SCDefn, packageName: String): Pair<String, ByteArray> {
    val className =
            ClassName(packageName, scDefn.first)

    return Pair(className.className, SCCompiler(className).sc(scDefn))
}

private fun compiledPrimitives(packageName: String): CompileOutput =
        listOf(
                compileBinaryOperatorBuiltIn(packageName, "Add", 2) { mv -> mv.generateAdd() },
                compileBinaryOperatorBuiltIn(packageName, "Sub", 2) { mv -> mv.generateSub() },
                compileBinaryOperatorBuiltIn(packageName, "Mul", 2) { mv -> mv.generateMul() },
                compileBinaryOperatorBuiltIn(packageName, "Div", 2) { mv -> mv.generateDiv() },
                compileBuiltIn(packageName, "Negate", 1) { mv ->
                    mv.generatePush(0)
                    mv.generateEval()
                    mv.generateNeg()
                    mv.generateUpdate(1)
                    mv.generatePop(1)
                    mv.generateUnwind()
                },
                compileBinaryOperatorBuiltIn(packageName, "EqEq", 2) { mv -> mv.generateEq() },
                compileBinaryOperatorBuiltIn(packageName, "TildeEq", 2) { mv -> mv.generateNe() },
                compileBinaryOperatorBuiltIn(packageName, "Lt", 2) { mv -> mv.generateLt() },
                compileBinaryOperatorBuiltIn(packageName, "LtEq", 2) { mv -> mv.generateLe() },
                compileBinaryOperatorBuiltIn(packageName, "Gt", 2) { mv -> mv.generateGt() },
                compileBinaryOperatorBuiltIn(packageName, "GtEq", 2) { mv -> mv.generateGe() },
                compileBuiltIn(packageName, "CondTrue", 0) { mv -> mv.generatePush(1) },
                compileBuiltIn(packageName, "CondFalse", 0) { mv -> mv.generatePush(2) },
                compileBuiltIn(packageName, "If", 3) { mv ->
                    mv.generatePush(0)
                    mv.generateEval()
                    mv.generateCond(ClassName(packageName, "CondTrue").descriptor, ClassName(packageName, "CondFalse").descriptor)
                    mv.generateUpdate(3)
                    mv.generatePop(3)
                    mv.generateUnwind()
                }
        )

fun compileBinaryOperatorBuiltIn(packageName: String, name: String, numberOfParameters: Int, instructions: (MethodVisitor) -> Unit): Pair<String, ByteArray> =
        compileBuiltIn(packageName, name, numberOfParameters) { it ->
            it.generatePush(1)
            it.generateEval()
            it.generatePush(1)
            it.generateEval()
            instructions(it)
            it.generateUpdate(2)
            it.generatePop(2)
            it.generateUnwind()
        }

fun compileBuiltIn(packageName: String, name: String, numberOfParameters: Int, instructions: (MethodVisitor) -> Unit): Pair<String, ByteArray> {
    val className =
            ClassName(packageName, name)

    return Pair(className.className, object : BuiltinCompiler(className) {
        override fun addInstructions(mv: MethodVisitor) {
            instructions(mv);
        }
    }.builtin(numberOfParameters))
}
