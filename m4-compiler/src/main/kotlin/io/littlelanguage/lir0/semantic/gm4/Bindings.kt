package io.littlelanguage.lir0.semantic.gm4

import io.littlelanguage.lir0.dynamic.ast.Name

public class Bindings(val bindings: Map<String, Int>) {
    fun deltaOffset(delta: Int) =
            Bindings(bindings.mapValues { it.value + delta })

    operator fun get(name: String): Int? =
            bindings[name]

    fun add(first: Name, index: Int): Bindings =
            Bindings(bindings + Pair(first, index))
}