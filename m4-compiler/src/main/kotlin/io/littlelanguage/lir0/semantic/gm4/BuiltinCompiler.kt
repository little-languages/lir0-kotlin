package io.littlelanguage.lir0.semantic.gm4

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

abstract class BuiltinCompiler(override val className: ClassName): ParentCompiler(className) {
    fun builtin(numberOfParameters: Int): ByteArray {
        cw.visit(Opcodes.V1_5, Opcodes.ACC_PUBLIC, className.descriptor, null, "java/lang/Object", arrayOf("io/littlelanguage/gmachine/m4/SC"))

        cw.visitField(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC + Opcodes.ACC_FINAL, "INSTANCE", "L${className.descriptor};", null, null).visitEnd()

        addClassConstructor()
        addDefaultConstructor()
        addToString()

        addNumberOfParameters(numberOfParameters)

        addRunMethod()

        cw.visitEnd()

        return cw.toByteArray()
    }

    private fun addRunMethod() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "run", "(Lio/littlelanguage/gmachine/m4/Machine;)V", null, null)

        addInstructions(mv)

        mv.visitInsn(Opcodes.RETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    abstract fun addInstructions(mv: MethodVisitor)
}
