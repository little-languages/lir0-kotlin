package io.littlelanguage.lir0.semantic.gm4

class ClassName(private val packageName: String, name: String) {
    val name =
            markupName(name.capitalize())

    private fun markupName(n: String) =
            n.map {
                when (it) {
                    '+' -> "Add"
                    '-' -> "Sub"
                    '*' -> "Mul"
                    '/' -> "Div"
                    '=' -> "Eq"
                    '~' -> "Tilde"
                    '<' -> "Lt"
                    '>' -> "Gt"
                    else -> it.toString()
                }
            }.joinToString("")

    val className: String
        get() = "$packageName.$name"

    val descriptor: String
        get() = "${packageName.replace('.', '/')}/$name"

    fun relativeClassName(name: String) =
            ClassName(packageName, name)
}