package io.littlelanguage.lir0.semantic.gm4

import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes

abstract class ParentCompiler(protected open val className: ClassName) {
    protected val cw =
            ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES)

    protected fun addClassConstructor() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "<clinit>", "()V", null, null)

        mv.visitCode()
        mv.visitTypeInsn(Opcodes.NEW, className.className.replace('.', '/'))
        mv.visitInsn(Opcodes.DUP)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, className.descriptor, "<init>", "()V", false)
        mv.visitFieldInsn(Opcodes.PUTSTATIC, className.descriptor, "INSTANCE", "L${className.descriptor};")
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(1, 1)
        mv.visitEnd()
    }

    protected fun addDefaultConstructor() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null)

        mv.visitCode()
        mv.visitVarInsn(Opcodes.ALOAD, 0)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false)
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(1, 1)
        mv.visitEnd()
    }

    protected fun addToString() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "toString", "()Ljava/lang/String;", null, null)

        mv.visitLdcInsn(className.name)
        mv.visitInsn(Opcodes.ARETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    protected fun addNumberOfParameters(numberOfParameters: Int) {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "numberOfParameters", "()I", null, null)

        mv.iconst(numberOfParameters)

        mv.visitInsn(Opcodes.IRETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }
}
