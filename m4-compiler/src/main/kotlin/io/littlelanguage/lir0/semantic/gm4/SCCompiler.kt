package io.littlelanguage.lir0.semantic.gm4

import io.littlelanguage.lir0.dynamic.ast.*
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

class SCCompiler(override val className: ClassName) : ParentCompiler(className) {
    fun sc(scDefn: SCDefn): ByteArray {
        cw.visit(Opcodes.V1_5, Opcodes.ACC_PUBLIC, className.descriptor, null, "java/lang/Object", arrayOf("io/littlelanguage/gmachine/m4/SC"))

        cw.visitField(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC + Opcodes.ACC_FINAL, "INSTANCE", "L${className.descriptor};", null, null).visitEnd()

        addClassConstructor()
        addDefaultConstructor()
        addToString()

        if (scDefn.first == "main") {
            addMainFunction()
        }

        addNumberOfParameters(scDefn.second.size)

        addRunMethod(scDefn.second, scDefn.third)

        cw.visitEnd()

        return cw.toByteArray()
    }

    private fun addMainFunction() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null)

        mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;")
        mv.visitTypeInsn(Opcodes.NEW, "io/littlelanguage/gmachine/m4/Machine")
        mv.visitInsn(Opcodes.DUP)
        mv.visitVarInsn(Opcodes.BIPUSH, 100)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "io/littlelanguage/gmachine/m4/Machine", "<init>", "(I)V", false)
        mv.visitFieldInsn(Opcodes.GETSTATIC, className.descriptor, "INSTANCE", "L${className.descriptor};")
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "run", "(Lio/littlelanguage/gmachine/m4/SC;)Lio/littlelanguage/gmachine/m4/Node;", false)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/Object;)V", false)
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun addRunMethod(names: List<String>, expr: Expr) {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "run", "(Lio/littlelanguage/gmachine/m4/Machine;)V", null, null)

        e(expr, Bindings(names.zip(0..names.size).toMap()), mv)
        mv.generateUpdate(names.size)
        mv.generatePop(names.size)
        mv.generateUnwind()
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun e(e: Expr, b: Bindings, mv: MethodVisitor): Unit =
            when (e) {
                is ENum -> mv.generatePushInt(e.value)
                is EAp -> {
                    e(e.e2, b, mv)
                    e(e.e1, b.deltaOffset(1), mv)
                    mv.generateMkAp()
                }
                is EVar -> {
                    val offset =
                            b[e.name]

                    if (offset == null) {
                        mv.generatePushGlobal(className.relativeClassName(e.name).descriptor)
                    } else
                        mv.generatePush(offset)
                }
                is ELet ->
                    when (e.isRec) {
                        IsRec.NonRecursive -> {
                            var bindings =
                                    b

                            e.decls.forEach {
                                e(it.second, bindings, mv)
                                bindings = bindings.deltaOffset(1)
                            }

                            e.decls.forEachIndexed { index, pair ->
                                bindings = bindings.add(pair.first, e.decls.size - index - 1)
                            }

                            e(e.expr, bindings, mv)

                            mv.generateSlide(e.decls.size)
                        }
                        IsRec.Recursive -> {
                            var bindings =
                                    b.deltaOffset(e.decls.size)

                            mv.generateAlloc(e.decls.size)

                            e.decls.forEachIndexed { index, pair ->
                                bindings = bindings.add(pair.first, e.decls.size - index - 1)
                            }

                            e.decls.forEachIndexed { index, pair ->
                                e(pair.second, bindings, mv)
                                mv.generateUpdate(e.decls.size - index - 1)
                            }

                            e(e.expr, bindings, mv)

                            mv.generateSlide(e.decls.size)
                        }
                    }
                else -> TODO(e.toString())
            }
}