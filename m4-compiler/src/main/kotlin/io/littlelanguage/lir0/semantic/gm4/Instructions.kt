package io.littlelanguage.lir0.semantic.gm4

import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

fun MethodVisitor.generateUpdate(n: Int) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.iconst(n)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "update", "(I)V", false)
}

fun MethodVisitor.generatePop(n: Int) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.iconst(n)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "pop", "(I)V", false)
}

fun MethodVisitor.generatePushInt(n: Int) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.iconst(n)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "pushInt", "(I)V", false)
}

fun MethodVisitor.generatePush(n: Int) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.iconst(n)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "push", "(I)V", false)
}

fun MethodVisitor.generatePushGlobal(scClassName: String) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitFieldInsn(Opcodes.GETSTATIC, scClassName, "INSTANCE", "L$scClassName;")
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "pushGlobal", "(Lio/littlelanguage/gmachine/m4/SC;)V", false)
}

fun MethodVisitor.generateUnwind() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "unwind", "()V", false)
}

fun MethodVisitor.generateMkAp() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "mkAp", "()V", false)
}

fun MethodVisitor.generateSlide(n: Int) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.iconst(n)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "slide", "(I)V", false)
}

fun MethodVisitor.generateAlloc(n: Int) {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.iconst(n)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "alloc", "(I)V", false)
}

fun MethodVisitor.generateEval() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "eval", "()V", false)
}

fun MethodVisitor.generateAdd() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "add", "()V", false)
}

fun MethodVisitor.generateSub() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "sub", "()V", false)
}

fun MethodVisitor.generateMul() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "mul", "()V", false)
}

fun MethodVisitor.generateDiv() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "div", "()V", false)
}

fun MethodVisitor.generateNeg() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "neg", "()V", false)
}

fun MethodVisitor.generateEq() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "eq", "()V", false)
}

fun MethodVisitor.generateNe() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "ne", "()V", false)
}

fun MethodVisitor.generateLt() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "lt", "()V", false)
}

fun MethodVisitor.generateLe() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "le", "()V", false)
}

fun MethodVisitor.generateGt() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "gt", "()V", false)
}

fun MethodVisitor.generateGe() {
    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "ge", "()V", false)
}

fun MethodVisitor.generateCond(i1ClassName: String, i2ClassName: String) {
    this.visitVarInsn(Opcodes.ALOAD, 1)

    this.visitVarInsn(Opcodes.ALOAD, 1)
    this.visitFieldInsn(Opcodes.GETSTATIC, i1ClassName, "INSTANCE", "L$i1ClassName;")
    this.visitFieldInsn(Opcodes.GETSTATIC, i2ClassName, "INSTANCE", "L$i2ClassName;")
    this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m4/Machine", "cond", "(Lio/littlelanguage/gmachine/m4/SC;Lio/littlelanguage/gmachine/m4/SC;)V", false)
}


fun MethodVisitor.iconst(n: Int) {
    when (n) {
        0 -> this.visitInsn(Opcodes.ICONST_0)
        1 -> this.visitInsn(Opcodes.ICONST_1)
        2 -> this.visitInsn(Opcodes.ICONST_2)
        3 -> this.visitInsn(Opcodes.ICONST_3)
        4 -> this.visitInsn(Opcodes.ICONST_4)
        5 -> this.visitInsn(Opcodes.ICONST_5)
        else -> this.visitVarInsn(Opcodes.BIPUSH, n)
    }
}
