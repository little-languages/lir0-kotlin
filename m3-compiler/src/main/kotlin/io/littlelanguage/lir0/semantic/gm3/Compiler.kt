package io.littlelanguage.lir0.semantic.gm3

import io.littlelanguage.data.Either
import io.littlelanguage.data.Right
import io.littlelanguage.lir0.Errors
import io.littlelanguage.lir0.dynamic.ast.*
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes

typealias CompileOutput =
        List<Pair<String, ByteArray>>

fun compile(p: Program, packageName: String): Either<List<Errors>, CompileOutput> =
        Right(Compiler(packageName).p(p))

private class Compiler(private val packageName: String) {
    fun p(p: Program): CompileOutput =
            (p + preludeDefs).map {
                val className = "$packageName.${it.first.capitalize()}"
                Pair(className, SCCompiler(packageName, className).sc(it))
            }

    private val preludeDefs =
            listOf(
                    Triple("I", listOf("x"), EVar("x")),
                    Triple("K", listOf("x", "y"), EVar("x")),
                    Triple("K1", listOf("x", "y"), EVar("y")),
                    Triple("S", listOf("f", "g", "x"), EAp(EAp(EVar("f"), EVar("x")), EAp(EVar("g"), EVar("x")))),
                    Triple("compose", listOf("f", "g", "x"), EAp(EVar("f"), EAp(EVar("g"), EVar("x")))),
                    Triple("twice", listOf("f"), EAp(EAp(EVar("compose"), EVar("f")), EVar("f")))
            )
}

private class SCCompiler(private val packageName: String, private val className: String) {
    private val cw =
            ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES)

    private val moduleName =
            className.replace('.', '/')

    fun sc(scDefn: SCDefn): ByteArray {
        cw.visit(Opcodes.V1_5, Opcodes.ACC_PUBLIC, className.replace('.', '/'), null, "java/lang/Object", arrayOf("io/littlelanguage/gmachine/m3/SC"))

        cw.visitField(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC + Opcodes.ACC_FINAL, "INSTANCE", "L$moduleName;", null, null).visitEnd()

        addClassConstructor()
        addDefaultConstructor()
        addToString(scDefn.first)

        if (scDefn.first == "main") {
            addMainFunction()
        }

        addNumberOfParameters(scDefn.second.size)

        addRunMethod(scDefn.second, scDefn.third)

        cw.visitEnd()

        return cw.toByteArray()
    }

    private fun addClassConstructor() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "<clinit>", "()V", null, null)

        mv.visitCode()
        mv.visitTypeInsn(Opcodes.NEW, className.replace('.', '/'))
        mv.visitInsn(Opcodes.DUP)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, moduleName, "<init>", "()V", false)
        mv.visitFieldInsn(Opcodes.PUTSTATIC, moduleName, "INSTANCE", "L$moduleName;")
        mv.visitInsn(Opcodes.RETURN)

        mv.visitMaxs(1, 1)
        mv.visitEnd()
    }

    private fun addDefaultConstructor() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null)

        mv.visitCode()
        mv.visitVarInsn(Opcodes.ALOAD, 0)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false)
        mv.visitInsn(Opcodes.RETURN)
        mv.visitMaxs(1, 1)
        mv.visitEnd()
    }

    private fun addMainFunction() {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL + Opcodes.ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null)

        mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;")
        mv.visitTypeInsn(Opcodes.NEW, "io/littlelanguage/gmachine/m3/Machine")
        mv.visitInsn(Opcodes.DUP)
        mv.visitVarInsn(Opcodes.BIPUSH, 100)
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "io/littlelanguage/gmachine/m3/Machine", "<init>", "(I)V", false)
        mv.visitFieldInsn(Opcodes.GETSTATIC, moduleName, "INSTANCE", "L$moduleName;")
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "run", "(Lio/littlelanguage/gmachine/m3/SC;)Lio/littlelanguage/gmachine/m3/Node;", false)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/Object;)V", false)

        mv.visitInsn(Opcodes.RETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun addToString(name: String) {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "toString", "()Ljava/lang/String;", null, null)

        mv.visitLdcInsn(name)
        mv.visitInsn(Opcodes.ARETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun addRunMethod(names: List<String>, expr: Expr) {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "run", "(Lio/littlelanguage/gmachine/m3/Machine;)V", null, null)

        e(expr, Bindings(names.zip(0..names.size).toMap()), mv)
        generateUpdate(names.size, mv)
        generatePop(names.size, mv)
        generateUnwind(mv)

        mv.visitInsn(Opcodes.RETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }

    private fun addNumberOfParameters(numberOfParameters: Int) {
        val mv =
                cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_FINAL, "numberOfParameters", "()I", null, null)

        iconst(numberOfParameters, mv)

        mv.visitInsn(Opcodes.IRETURN)
        mv.visitMaxs(2, 1)
        mv.visitEnd()
    }


    private fun e(e: Expr, b: Bindings, mv: MethodVisitor): Unit =
            when (e) {
                is ENum -> generatePushInt(e.value, mv)
                is EAp -> {
                    e(e.e2, b, mv)
                    e(e.e1, b.deltaOffset(1), mv)
                    generateMkAp(mv)
                }
                is EVar -> {
                    val offset =
                            b[e.name]

                    if (offset == null)
                        generatePushGlobal(e.name, mv)
                    else
                        generatePush(offset, mv)
                }
                is ELet -> {
                    if (e.isRec == IsRec.NonRecursive) {
                        var bindings = b

                        e.decls.forEach {
                            e(it.second, bindings, mv)
                            bindings = bindings.deltaOffset(1)
                        }

                        e.decls.forEachIndexed { index, pair ->
                            bindings = bindings.add(pair.first, e.decls.size - index - 1)
                        }

                        e(e.expr, bindings, mv)

                        generateSlide(e.decls.size, mv)
                    } else {
                        var bindings = b.deltaOffset(e.decls.size)

                        generateAlloc(e.decls.size, mv)

                        e.decls.forEachIndexed { index, pair ->
                            bindings = bindings.add(pair.first, e.decls.size - index - 1)
                        }

                        e.decls.forEachIndexed { index, pair ->
                            e(pair.second, bindings, mv)
                            generateUpdate(e.decls.size - index - 1, mv)
                        }

                        e(e.expr, bindings, mv)

                        generateSlide(e.decls.size, mv)
                    }
                }
                else -> TODO(e.toString())
            }


    private fun generateUpdate(n: Int, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        iconst(n, mv)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "update", "(I)V", false)
    }

    private fun generatePop(n: Int, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        iconst(n, mv)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "pop", "(I)V", false)
    }

    private fun generatePushInt(n: Int, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        iconst(n, mv)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "pushInt", "(I)V", false)
    }

    private fun generatePush(n: Int, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        iconst(n, mv)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "push", "(I)V", false)
    }

    private fun generatePushGlobal(name: String, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        mv.visitFieldInsn(Opcodes.GETSTATIC, "${packageName.replace('.', '/')}/${name.capitalize()}", "INSTANCE", "L${packageName.replace('.', '/')}/${name.capitalize()};")
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "pushGlobal", "(Lio/littlelanguage/gmachine/m3/SC;)V", false)
    }

    private fun generateUnwind(mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "unwind", "()V", false)
    }

    private fun generateMkAp(mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "mkAp", "()V", false)
    }

    private fun generateSlide(n: Int, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        iconst(n, mv)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "slide", "(I)V", false)
    }

    private fun generateAlloc(n: Int, mv: MethodVisitor) {
        mv.visitVarInsn(Opcodes.ALOAD, 1)
        iconst(n, mv)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "io/littlelanguage/gmachine/m3/Machine", "alloc", "(I)V", false)
    }
}


private fun iconst(n: Int, mv: MethodVisitor) {
    when (n) {
        0 -> mv.visitInsn(Opcodes.ICONST_0)
        1 -> mv.visitInsn(Opcodes.ICONST_1)
        2 -> mv.visitInsn(Opcodes.ICONST_2)
        3 -> mv.visitInsn(Opcodes.ICONST_3)
        4 -> mv.visitInsn(Opcodes.ICONST_4)
        5 -> mv.visitInsn(Opcodes.ICONST_5)
        else -> mv.visitVarInsn(Opcodes.BIPUSH, n)
    }
}

private class Bindings(val bindings: Map<String, Int>) {
    fun deltaOffset(delta: Int) =
            Bindings(bindings.mapValues { it.value + delta })

    operator fun get(name: String): Int? =
            bindings[name]

    fun add(first: Name, index: Int): Bindings =
            Bindings(bindings + Pair(first, index))
}
