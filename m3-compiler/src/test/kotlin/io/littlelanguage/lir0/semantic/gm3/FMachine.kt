package io.littlelanguage.lir0.semantic.gm3

import io.littlelanguage.gmachine.m3.*

class FMachine(stackSize: Int) : Machine(stackSize) {
    override fun mkAp() {
        printStack("MkAp")
        super.mkAp()
    }

    override fun push(n: Int) {
        printStack("Push $n")
        super.push(n)
    }

    override fun pushGlobal(sc: SC) {
        printStack("PushGlobal $sc")
        super.pushGlobal(sc)
    }

    override fun pushInt(n: Int) {
        printStack("PushInt $n")
        super.pushInt(n)
    }

    override fun update(n: Int) {
        printStack("Update $n")
        super.update(n)
    }

    override fun pop(n: Int) {
        printStack("Pop $n")
        super.pop(n)
    }

    override fun unwindNInd(n: Node) {
        printStack("Unwind")
        super.unwindNInd(n)
    }

    override fun unwindNAp(n: NAp) {
        printStack("Unwind")
        super.unwindNAp(n)
    }

    override fun unwindNGlobal(n: NGlobal) {
        printStack("Unwind")
        super.unwindNGlobal(n)
    }

    override fun unwindNNum(n: NNum) {
        printStack("Unwind")
        super.unwindNNum(n)
    }
}
