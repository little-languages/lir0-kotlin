package io.littlelanguage.lir0.semantic.gm1

import io.kotest.core.spec.style.FunSpec
import io.kotest.core.spec.style.FunSpecDsl
import io.kotest.matchers.shouldBe
import io.littlelanguage.data.Either
import io.littlelanguage.data.Left
import io.littlelanguage.data.Right
import io.littlelanguage.gmachine.m1.NNum
import io.littlelanguage.gmachine.m1.SC
import io.littlelanguage.lir0.Errors
import io.littlelanguage.lir0.bin.MyClassLoader
import io.littlelanguage.lir0.dynamic.translate
import io.littlelanguage.lir0.lexer.LA
import org.yaml.snakeyaml.Yaml
import java.io.File
import java.io.StringReader

private const val PACKAGE_NAME =
        "io.littlelanguage.lir0.semantic.gm1"

private val yaml = Yaml()

// This is horrid and for debugging purposes - I want the binary as they are being compiled so that I can
// disassemble them when the wheels come off.
private var testCount =
        0

class SemanticTests : FunSpec({
    context("Conformance Tests") {
//        val content = File("./../overview/docs/p0/conformance/semantics.yaml").readText()
//        val content = File("./m1.yaml").readText()
        val content = khttp.get("https://little-languages.gitlab.io/overview/lir0/conformance/m1.yaml").text

        val scenarios: Any = yaml.load(content)

        if (scenarios is List<*>) {
            conformanceTest(this, scenarios)
        }
    }
})


fun parse(input: String, packageName: String): Either<List<Errors>, CompileOutput> =
        io.littlelanguage.lir0.static.parse(LA(StringReader(input)))
                .mapLeft { listOf(it) }
                .map { translate(it) }
                .andThen { compile(it, packageName) }

fun run(input: String, packageName: String): Either<List<Errors>, Pair<Int, Int>> =
        parse(input, packageName).map {
            val myClassLoader =
                    MyClassLoader()

            it.forEach { sc ->
                File("build/classes/kotlin/test/" + sc.first.replace('.', '/').dropLastWhile { c -> c != '/' }.dropLast(1)).mkdirs()
                File("build/classes/kotlin/test/${sc.first.replace('.', '/')}.class").writeBytes(sc.second)
                myClassLoader.defineClass(sc.first, sc.second)
            }

            val instanceField =
                    myClassLoader.loadClass("$packageName.Main").getDeclaredField("INSTANCE")

            val instanceValue =
                    instanceField.get(null) as SC

            val machine =
                    FMachine(100)

            val node =
                    machine.run(instanceValue)

            Pair((node as NNum).n, machine.steps)
        }


suspend fun conformanceTest(ctx: FunSpecDsl.ContextScope, scenarios: List<*>) {
    scenarios.forEach { scenario ->
        val s = scenario as Map<*, *>

        val nestedScenario = s["scenario"] as Map<*, *>?
        if (nestedScenario == null) {
            val name = s["name"] as String
            val input = s["input"] as String
            val output = s["output"]

            ctx.test(name) {
                val packageName =
                        "$PACKAGE_NAME.p$testCount"

                testCount += 1

                val parseResult =
                        run(input, packageName)


                val lhs =
                        when (parseResult) {
                            is Left -> Pair(0, 0)

                            is Right ->
                                parseResult.right
                        }

                val rhs =
                        output as Map<*, *>


                mapOf(
                        Pair("result", lhs.first.toString()),
                        Pair("steps", lhs.second)
                ) shouldBe rhs
            }
        } else {
            val name = nestedScenario["name"] as String
            val tests = nestedScenario["tests"] as List<*>
            ctx.context(name) {
                conformanceTest(this, tests)
            }
        }
    }
}
