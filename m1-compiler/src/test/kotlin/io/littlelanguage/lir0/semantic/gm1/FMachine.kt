package io.littlelanguage.lir0.semantic.gm1

import io.littlelanguage.gmachine.m1.*

class FMachine(stackSize: Int) : Machine(stackSize) {
    override fun mkAp() {
        printStack("MkAp")
        super.mkAp()
    }

    override fun push(n: Int) {
        printStack("Push $n")
        super.push(n)
    }

    override fun pushGlobal(sc: SC) {
        printStack("PushGlobal $sc")
        super.pushGlobal(sc)
    }

    override fun pushInt(n: Int) {
        printStack("PushInt $n")
        super.pushInt(n)
    }

    override fun slide(n: Int) {
        printStack("Slide $n")
        super.slide(n)
    }

    override fun unwindNAp(n: NAp) {
        printStack("Unwind")
        super.unwindNAp(n)
    }

    override fun unwindNGlobal(n: NGlobal) {
        printStack("Unwind")
        super.unwindNGlobal(n)
    }

    override fun unwindNNum(n: NNum) {
        printStack("Unwind")
        super.unwindNNum(n)
    }
}